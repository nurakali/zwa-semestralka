# Popis

Aplikace zobrazuje seznam úkolů, který je dostupný pro veřejnost. Slouží ke sdílení stavu projektu na veřejnost. Příkladem projektu by mohla být organizace divadelního festivalu Improtřesk, kde účastnící čekají na drobné organizační milníky, jako je třeba kompletace lektorů, workshopů, účinkujících, programu, harmonogramu, a podobně.

# Uživatelské role

* Návštěvník (nepřihlášený uživatel)
* Uživatel (po přihlášení)
* Admin (s extra pravomocemi)

# Funkce

Návštěvník vidí splněné a nesplněné úkoly a jejich postupné změny, tak, jak šly v čase. Uživatelé mohou přidávat a editovat svoje úkoly; komentovat úkoly svoje a ostatních. Administrátor, má otevřené všechny možnosti bez omezení.

## Stack

PHP, Databáze (podle toho co bude dostupné), HTML 5, Bootstrap 4, OpenGraph, jQuery, git

---

## Akceptační podmínky

### Úvodní stránka

Na úvodní stránce se zobrazuje omezený počet úkolů seřazených podle poslední změny s možností stránkování a progress bar celého projektu. Návštěvník může úkoly procházet do historie a filtrovat podle stavu, názvu a značky.

### Detail úkolu

Každý úkol má stránku detailu, kde uživatel uvidí historii úkolu a diskuzi.

### Přihlášení uživatele

Uživatel vyplní e-mail a heslo. Formulář obsahuje validace na vyplnění obou položek.

### Seznam úkolů (pouze uživatel)

Uživatel vidí přehled svých úkolů. Má možnost smazat úkol.

### Přidání a editace úkolu (pouze uživatel)

Uživatel přidává nový úkol nebo edituje již existující. Formulář validuje vyplnění všech údajů a jedinečnost názvu úkolu.

### Změna hesla (pouze uživatel)

Uživatel si mění heslo. Formulář kontroluje, že uživatel vyplnil heslo dvakrát správně do input[type=password]

### Seznam uživatelů

Seznam všech uživatelů v systému, možnost mazání uživatele. Přístupné pouze pro administrátory.

### Přidání a změna uživatele (pouze admin)

Formulář pro přidání uživatele. Admin vyplňuje jméno a e-mail, heslo se odesílá e-mailem. Formulář validuje vyplnění všech polí a platnost e-mailové adresy. Přístupné pouze pro administrátory.
