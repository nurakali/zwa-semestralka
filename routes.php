<?php

/**
 * Return route configuration for Defiant framework.
 *
 * @return mixed[]
 */
function getRoutes() {
  return [
    ['get', '/', ['\\Totodo\\View\\Home', 'index'], 'home'],
    [['get', 'post'], '/login', ['\\Totodo\\View\\Login', 'index'], 'login'],
    ['get', '/logout', ['\\Totodo\\View\\Login', 'logout'], 'logout'],
    ['get', '/tasks', ['\\Totodo\\View\\Tasks', 'index'], 'task:list'],
    [['get', 'post'], '/tasks/create', ['\\Totodo\\View\\TaskOperations', 'edit'], 'task:create'],
    ['get', '/tasks/:taskId', ['\\Totodo\\View\\Tasks', 'detail'], 'task:detail'],
    [['get', 'post'], '/tasks/:taskId/comment', ['\\Totodo\\View\\TaskOperations', 'comment'], 'task:comment'],
    [['get', 'post'], '/tasks/:taskId/assign', ['\\Totodo\\View\\TaskOperations', 'assign'], 'task:assign'],
    ['get', '/tasks/:taskId/comments', ['\\Totodo\\View\\Tasks', 'taskComments'], 'task:comments'],
    ['get', '/tasks/:taskId/history', ['\\Totodo\\View\\TaskOperations', 'history'], 'task:history'],
    ['get', '/tasks/:taskId/archive', ['\\Totodo\\View\\TaskOperations', 'archive'], 'task:status:archive'],
    ['get', '/tasks/:taskId/unarchive', ['\\Totodo\\View\\TaskOperations', 'unarchive'], 'task:status:unarchive'],
    [['get', 'post'], '/tasks/:taskId/edit', ['\\Totodo\\View\\TaskOperations', 'edit'], 'task:edit'],
    ['get', '/tasks/:taskId/status/:statusId', ['\\Totodo\\View\\TaskOperations', 'changeStatus'], 'task:status:change'],
    ['get', '/user', ['\\Totodo\\View\\User', 'index'], 'self:home'],
    [['get', 'post'], '/user/edit', ['\\Totodo\\View\\User', 'edit'], 'self:edit'],
    [['get', 'post'], '/user/changePassword', ['\\Totodo\\View\\User', 'changePassword'], 'self:password:change'],
    ['get', '/user/tasks', ['\\Totodo\\View\\User', 'tasks'], 'self:tasks'],
    ['get', '/users', ['\\Totodo\\View\\Users', 'index'], 'user:list'],
    [['get', 'post'], '/users/create', ['\\Totodo\\View\\UserOperations', 'edit'], 'user:create'],
    ['get', '/users/:userId', ['\\Totodo\\View\\Users', 'detail'], 'user:detail'],
    ['get', '/users/:userId/tasks', ['\\Totodo\\View\\Users', 'tasks'], 'user:tasks'],
    [['get', 'post'], '/users/:userId/edit', ['\\Totodo\\View\\UserOperations', 'edit'], 'user:edit'],
    [['get', 'post'], '/users/:userId/password', ['\\Totodo\\View\\UserOperations', 'changePassword'], 'user:password'],
    ['get', '/users/:userId/delete', ['\\Totodo\\View\\UserOperations', 'delete'], 'user:delete'],
    ['get', '/api/taskStats', ['\\Totodo\\View\\TaskWeekStats'], 'api:task:stats'],
  ];
}
