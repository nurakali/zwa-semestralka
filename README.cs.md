# Totodo

Totodo, čteno [:tu-tu-dů:], je malinkatý, skoro až titěrný manažer pro správu úkolů v malém týmu. Jedna z jeho možných aplikací je správa úkolů při soužití v domácnosti, např. "Co je potřeba nakoupit", "Co je potřeba opravit" nebo "Co je potřeba připravit na plánovanou megapárty".

Pokud jste přišel za účelem hodnocení, mohla by vás zajímat [Okomentovaná hodnotící tabulka](./hodnotici-tabulka.md) z kurzu ZWA, [zadání projektu](./zadani.md) a také [běžící plikace](https://wa.toad.cz/~zakpave4/totodo) je publikovaná na serveru wa.toad.cz.

## Funkce

* Vytváření uživatelů
* Vytváření a editace úkolů
* Popis úkolů za použití jazyka Markdown
* Přiřazování úkolů uživatelů
* Určování stavu úkolů (Todo, In Progress, Hotovo)
* Archivace dokončených úkolů
* Zobrazení grafu nedávné aktivity

## Požadavky

* PHP 7.0 nebo vyšší
* Composer pro instalaci závislostí

## Instalace

Nejjednodušší cesta je za použití správce balíčků Composer. Pokud jej ještě nemáte, můžete si ho stáhnout a nainstalovat pomocí jeho [uživatelské příručky](https://doc.nette.org/composer). Poté použijte příkaz:

```php
composer install
```

pokud potřebujete vygenerovat dokumentaci, je potřeba stáhnout i takzvané "development dependencies", které obsahují PHPDocumentor. To provedete následujícím příkazem:

```php
composer install --dev
```

## Konfigurace

Totodo vyhledává ve svém kořenovém adresáři soubor localSettings.php. Pro změnu nastavení jej vytvořte a můžete přepisovat veškerá nastavení frameworku Defiant, na kterém je Totodo postaveno.

Doporučuji minimálně nastavit tajný klíč aplikace, který ovlivňuje šifrování hesel.

Příklad:

```php
// localSettings.php
$defiantSettings = [
  'secretKey' => 'my-own-secret-key',
];
```

## Nastavení databáze

Nejprve vytvoříte databázovou strukturu, to způsobí, že se podle vašich nastavení vytvoří prázdná datbáze.

```php
./vendor/bin/defiant db:sync
```

Abyste aplikaci vůbec mohli použít, je potřeba nalít do ní základní dataset. To provedete za pomoci příkazu:

```php
./vendor/bin/defiant db:seed initialData.json
```

## Nastavení webového serveru

Nejrychlejší cesta jak Totodo spustit je za pomoci integrovaného PHP serveru:

```bash
php -S localhost:8000
```

Poté navštivte `http://localhost:8000` ve vašem prohlížeči a otevře se vám úvodní stránka aplikace.

# Architektura

Aplikace je postavená na MVC frameworku Defiant, kde Modely představují schéma perzistentní vrstvy (M), ViewClasses, představují kontrolery (C) a šablony představují views.

## Formuláře

Formuláře jsou obsluhováný za pomocí validací definovaných podle Modelů.
