# Totodo

'Totodo [:tu-tu-dů:] is a tiny task manager that helps you keep track of everyday tasks in small teams.

See [Czech README](./README.cs.md) or a [Running application](https://wa.toad.cz/~zakpave4/totodo).

## Installation

The best way to install Web Project is using Composer. If you don't have Composer yet,
download it following [the instructions](https://doc.nette.org/composer). Then use command:

```php
composer install
```

or if you want to generate documentation, run

```php
composer install --dev
```

## Database setup

First create database structure

```php
./vendor/bin/defiant db:sync
```

Second, seed initial data into this freshly created database

```php
./vendor/bin/defiant db:seed initialData.json
```

## Web Server Setup

The simplest way to get started is to start the built-in PHP server in the root directory of your project:

```bash
php -S localhost:8000
```

Then visit `http://localhost:8000` in your browser to see the welcome page.

## Requirements
------------

PHP 5.6 or higher, Composer.
