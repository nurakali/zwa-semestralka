<?php

/**
 * Manage logged in user
 */

namespace Totodo\View;

/**
 * View class for User account changes
 */
class User extends \Defiant\View {
  /**
   * Page size for task list
   */
  const TASKS_PAGE_SIZE = 10;

  /**
   * View API method determining accessibility of this view. Grants access
   * only to logged in users.
   *
   * @return bool
   */
  public function isAccessible() {
    return $this->getUser();
  }

  /**
   * Displays user list with paging
   *
   * @return string
   */
  public function index() {
    $userTasksMax = 7;
    $userDetail = $this->getUser();
    $tasksQuery = $userDetail->tasks->filter(['archived' => false]);
    $userTasks = $tasksQuery->clone()->limit(0, $userTasksMax)->all();
    $userTasksTotal = $tasksQuery->count();
    $recentHistory = $userDetail->changes->limit(0, 15)->all();
    return $this->renderTemplate('users/detail.pug', [
      'message' => $this->request->query('message', null),
      'recentHistory' => $recentHistory,
      'userDetail' => $userDetail,
      'userTasks' => $userTasks,
      'userTasksMax' => $userTasksMax,
      'userTasksTotal' => $userTasksTotal,
    ]);
  }


  /**
   * Render edit user form, create and edit users. Saves changes to database
   * and redirects back to user detail with result message.
   *
   * @throws Defiant\Http\NotFound when userId is passed, but user does not exist
   * @return string|void Returns rendered create/edit page or nothing on
   *   redirect
   */
  public function edit() {
    $user = $this->getUser();
    $values = [
      'name' => $user->name,
      'email' => $user->email,
      'theme' => $user->theme,
    ];
    $errors = [];

    if ($this->request->getMethod() === 'post') {
      $form = \Defiant\View\GenericForm::fromModel($this->models->user->model, ['name', 'email', 'theme']);
      try {
        $values = $form->getValidatedData($this->request);
      } catch(\Defiant\View\ValidationErrorCollection $e) {
        $errors = $e->toErrorList();
        $values = $e->getRawData();
      }

      if (sizeof($errors) === 0) {
        $user->setState($values);
        $user->save();
        return $this->redirect($this->url('self:home', [
          'message' => 'changed',
        ]));
      }
    }

    return $this->renderTemplate('users/edit-self.pug', [
      'errors' => $errors,
      'userItem' => $user,
      'themeOptions' => \Totodo\Model\User::THEMES,
      'values' => $values,
    ]);
  }

  /**
   * Displays list of tasks assigned to the user with paging
   *
   * @return string
   */
  public function tasks() {
    return \Totodo\View\Tasks::taskList(
      $this,
      $this->request,
      $this->models,
      $this->getUser()->tasks
    );
  }

  /**
   * Render change user password form and change users password. Saves changes
   * to database and redirects back to user detail with result message.
   *
   * @return string|void Returns rendered create/edit page or nothing on
   *   redirect
   */
  public function changePassword() {
    $user = $this->getUser();
    $values = [];
    $errors = [];

    if ($this->request->getMethod() === 'post') {
      $form = new \Totodo\Form\ChangePassword($user);
      try {
        $values = $form->getValidatedData($this->request);
      } catch(\Defiant\View\ValidationErrorCollection $e) {
        $errors = $e->toErrorList();
        $values = $e->getRawData();
      }

      if (sizeof($errors) === 0) {
        $user->password = $values['newPassword'];
        $user->save();
        return $this->redirect($this->url('self:home', [
          'message' => 'changed-password',
        ]));
      }
    }

    return $this->renderTemplate('users/edit-self-password.pug', [
      'errors' => $errors,
      'values' => $values,
    ]);
  }
}
