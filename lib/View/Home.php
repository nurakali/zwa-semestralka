<?php

/**
 * Display home page
 */

namespace Totodo\View;

/**
 * View class for home page
 */
class Home extends \Defiant\View {
  /**
   * View method for home page. Renders home template written in Pug language.
   * Queries the database for recent tasks and task history.
   *
   * @return string
   */
  public function index() {
    $recentTasks = $this->models->task->objects
      ->filter(['archived' => false])
      ->orderBy('-createdAt, -id')
      ->limit(0, 5)
      ->all();
    $recentHistory = $this->models->taskChange->objects
      ->orderBy('-createdAt, -id')
      ->limit(0, 15)
      ->all();
    return $this->renderTemplate('home.pug', [
      "recentHistory" => $recentHistory,
      "recentTasks" => $recentTasks,
    ]);
  }
}
