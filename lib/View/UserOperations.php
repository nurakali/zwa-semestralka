<?php

/**
 * Manage users
 */

namespace Totodo\View;

/**
 * View class for managing Users
 */
class UserOperations extends \Defiant\View {
  const FORM_FIELDS = [
    'name',
    'email',
    'roles',
  ];

  /**
   * View API method determining accessibility of this view. Grants access
   * only to users with permission 'manage-users'.
   *
   * @return bool
   */
  public function isAccessible() {
    $user = $this->getUser();
    return $user && $user->hasPerm('manage-users');
  }

   /**
    * View helper method that reads userId from request params and finds the
    * corresponding User.
    *
    * @throws Defiant\Http\NotFound when the ID is empty or user does not exist
    * @return Totodo\Model\User
    */
   protected function fetchUser() {
     $userId = $this->getParam('userId');
     $user = null;
     if ($userId) {
       $user = $this->models->user->objects->find($userId);
     }
     if (!$userId || !$user) {
       throw new \Defiant\Http\NotFound();
     }
     return $user;
   }

  /**
   * Render edit user form, create and edit users. Saves changes to database
   * and redirects back to user detail with result message.
   *
   * @throws Defiant\Http\NotFound when userId is passed, but user does not exist
   * @return string|void Returns rendered create/edit page or nothing on
   *   redirect
   */
  public function edit() {
    $mode = 'create';
    $values = ['roles' => []];
    $userId = $this->request->getParam('userId', null);
    $user = null;
    if ($userId) {
      $mode = 'edit';
      $user = $this->fetchUser();
      $values = $user->serializeFields(static::FORM_FIELDS);
    }
    $errors = [];
    $method = $this->request->getMethod();

    if ($method === 'post') {
      $form = \Defiant\View\GenericForm::fromModel($this->models->user->model, static::FORM_FIELDS);
      try {
        $values = $form->getValidatedData($this->request);
      } catch(\Defiant\View\ValidationErrorCollection $e) {
        $errors = $e->toErrorList();
        $values = $e->getRawData();
      }

      if (sizeof($errors) === 0) {
        if ($mode == 'edit') {
          $message = 'changed';
          $user->setState($values);
        } else {
          $message = 'created';
          $user = $this->models->user->create($values);
        }
        $user->save();
        return $this->redirect($this->url('user:detail', [
          'userId' => $user->id,
          'message' => $message,
        ]));
      }
    } else {
      $queryTags = $this->request->query('tags', null);
      if ($queryTags) {
        $values['tags'] = $queryTags;
      }
    }

    $rolesAll = $this->models->role->objects->orderBy('name')->all();
    $roleOptions = [];

    foreach ($rolesAll as $role) {
      $roleOptions[] = [
        'label' => $role->name,
        'value' => $role->id,
      ];
    }

    return $this->renderTemplate('users/edit.pug', [
      'errors' => $errors,
      'roleOptions' => $roleOptions,
      'userItem' => $user,
      'values' => $values,
    ]);
  }

  /**
   * Render change password form and change users password. Saves changes to
   * database and redirects back to user detail with result message.
   *
   * @throws Defiant\Http\NotFound when userId is passed, but user does not exist
   * @return string|void Returns rendered create/edit page or nothing on
   *   redirect
   */
  public function changePassword() {
    $user = $this->fetchUser();
    $values = [];
    $errors = [];
    $method = $this->request->getMethod();

    if ($method === 'post') {
      $form = new \Totodo\Form\ChangePassword($user, false);
      try {
        $values = $form->getValidatedData($this->request);
      } catch(\Defiant\View\ValidationErrorCollection $e) {
        $errors = $e->toErrorList();
        $values = $e->getRawData();
      }

      if (sizeof($errors) === 0) {
        $user->password = $values['newPassword'];
        $user->save();
        return $this->redirect($this->url('user:detail', [
          'userId' => $user->id,
          'message' => 'changed',
        ]));
      }
    }

    return $this->renderTemplate('users/edit-password.pug', [
      'errors' => $errors,
      'userItem' => $user,
    ]);
  }

  /**
   * Delete user and redirect back to user list.
   *
   * @throws Defiant\Http\NotFound when user does not exist
   * @return void
   */
  public function delete() {
    $user = $this->fetchUser();
    $user->delete();
    return $this->redirect($this->url('user:list', ['message' => 'deleted']));
  }
}
