<?php

/**
 * Return task statistics
 */

namespace Totodo\View;

/**
 * View class for weekly task statistics
 */
class TaskWeekStats extends \Defiant\View\Api {
  const DATE_FORMAT = 'Y-m-d';
  /**
   * Return task statistics for last week
   *
   * @return string
   */
  public function view() {
    $date = (new \DateTime())->sub(new \DateInterval('P6D'));
    $data = [];
    $connector = $this->models->taskChange;
    $model = $connector->model;

    for ($i = 0; $i < 7; $i++) {
      $day = $date->format(static::DATE_FORMAT);
      $tomorrow = clone $date;
      $tomorrow = $tomorrow->add(new \DateInterval('P1D'));
      $dayFilter = [
        'createdAt__gte' => $day,
        'createdAt__lt' => $tomorrow->format(static::DATE_FORMAT),
      ];
      $created = $connector->objects->filter($dayFilter)->filter(['type' => $model::ACTION_CREATE])->count();
      $updated = $connector->objects->filter($dayFilter)->filter(['type' => $model::ACTION_CHANGE])->count();
      $archived = $connector->objects->filter($dayFilter)->filter(['type' => $model::ACTION_ARCHIVE])->count();
      $commented = $connector->objects->filter($dayFilter)->filter(['type' => $model::ACTION_COMMENT])->count();
      $day = [
        'date' => $day,
        'created' => $created,
        'updated' => $updated,
        'archived' => $archived,
        'commented' => $commented,
      ];
      $data[] = $day;
      $date = $tomorrow;
    }
    return $this->render([
      'data' => $data,
    ]);
  }
}
