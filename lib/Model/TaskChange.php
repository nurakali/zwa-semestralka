<?php

/**
 * TaskChange model
 */
namespace Totodo\Model;

/**
 * TaskChange model used for storing task changes
 */
class TaskChange extends \Defiant\Model {
  const ACTION_CREATE = 1;
  const ACTION_CHANGE = 2;
  const ACTION_DELETE = 3;
  const ACTION_CHANGE_ASSIGN = 4;
  const ACTION_CHANGE_STATUS = 5;
  const ACTION_ARCHIVE = 6;
  const ACTION_UNARCHIVE = 7;
  const ACTION_COMMENT = 8;

  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'task' => [
      "type" => '\Defiant\Model\ForeignKeyField',
      "model" => "Totodo\Model\Task",
      "relatedName" => "changes",
    ],
    'author' => [
      "type" => '\Defiant\Model\ForeignKeyField',
      "model" => "Totodo\Model\User",
      "relatedName" => "changes",
    ],
    'type' => '\Defiant\Model\PositiveIntegerField',
    'previousState' => '\Defiant\Model\JsonField',
    'currentState' => '\Defiant\Model\JsonField',
  ];
}
