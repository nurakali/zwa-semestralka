<?php

/**
 * User Role model
 */
namespace Totodo\Model;

/**
 * User Role model used for assigning privileges
 * to users.
 */
class Role extends \Defiant\Model {
  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'name' => '\Defiant\Model\VarcharField',
    'isAdmin' => '\Defiant\Model\BooleanField',
    'users' => [
      'type' => '\Defiant\Model\ManyToManyField',
      'fk' => 'role',
      'model' => 'Totodo\Model\User',
      'trough' => 'Totodo\Model\UserRole',
      'via' => 'user',
    ],
    'permissions' => [
      'type' => '\Defiant\Model\OneToManyField',
      'fk' => 'role',
      'model' => '\Totodo\Model\Permission',
    ],
  ];
}
