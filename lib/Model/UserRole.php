<?php

/**
 * UserRole model
 */
namespace Totodo\Model;

/**
 * UserRole model used for connecting users and roles
 */
class UserRole extends \Defiant\Model {
  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'user' => [
      'type' => '\Defiant\Model\ForeignKeyField',
      'model' => '\Totodo\Model\User',
    ],
    'role' => [
      'type' => '\Defiant\Model\ForeignKeyField',
      'model' => '\Totodo\Model\Role',
    ],
  ];
}
