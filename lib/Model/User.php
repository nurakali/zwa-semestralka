<?php

/**
 * User model
 */
namespace Totodo\Model;

/**
 * User model used for storing users
 */
class User extends \Defiant\Model\Authenticateable {
  const FIELD_USERNAME = 'email';

  const THEMES = [
    [
      'label' => 'Cerulean',
      'value' => 'cerulean',
    ],
    [
      'label' => 'Cosmo',
      'value' => 'cosmo',
    ],
    [
      'label' => 'Cyborg',
      'value' => 'cyborg',
    ],
    [
      'label' => 'Darkly',
      'value' => 'darkly',
    ],
    [
      'label' => 'Flatly',
      'value' => 'flatly',
    ],
    [
      'label' => 'Journal',
      'value' => 'journal',
    ],
    [
      'label' => 'Litera',
      'value' => 'litera',
    ],
    [
      'label' => 'Lux',
      'value' => 'lux',
    ],
    [
      'label' => 'Materia',
      'value' => 'materia',
    ],
    [
      'label' => 'Minty',
      'value' => 'minty',
    ],
    [
      'label' => 'Pulse',
      'value' => 'pulse',
    ],
    [
      'label' => 'Sandstone',
      'value' => 'sandstone',
    ],
    [
      'label' => 'Simplex',
      'value' => 'simplex',
    ],
    [
      'label' => 'Slate',
      'value' => 'slate',
    ],
    [
      'label' => 'Solar',
      'value' => 'solar',
    ],
    [
      'label' => 'Space Lab',
      'value' => 'spacelab',
    ],
    [
      'label' => 'Superhero',
      'value' => 'superhero',
    ],
    [
      'label' => 'United',
      'value' => 'united',
    ],
    [
      'label' => 'Yeti',
      'value' => 'yeti',
    ],
  ];

  /** @var string[] $cachedPerms Cache for loaded permissions */
  protected $cachedPerms = null;

  /** @var \Totodo\Model\Role[] $cachedRoles Cache for loaded roles */
  protected $cachedRoles = null;

  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'name' => '\Defiant\Model\VarcharField',
    'email' => '\Defiant\Model\EmailField',
    'password' => [
      'type' => '\Defiant\Model\PasswordField',
      'isNull' => true,
    ],
    'lastLogin' => [
      'type' => '\Defiant\Model\DatetimeField',
      'isNull' => true,
    ],
    'theme' => [
      'type' => '\Defiant\Model\VarcharField',
      'isNull' => true,
    ],
    'roles' => [
      'type' => '\Defiant\Model\ManyToManyField',
      'fk' => 'user',
      'model' => 'Totodo\Model\Role',
      'trough' => 'Totodo\Model\UserRole',
      'via' => 'role',
    ],
    'changes' => [
      'type' => '\Defiant\Model\OneToManyField',
      'fk' => 'author',
      'model' => 'Totodo\Model\TaskChange',
    ],
    'tasks' => [
      'type' => '\Defiant\Model\OneToManyField',
      'fk' => 'assigned',
      'model' => 'Totodo\Model\Task',
    ],
  ];

  /**
   * Cache all permissions from the database in the instance properties
   *
   * @return void
   */
  protected function cachePerms() {
    if (!$this->cachedPerms) {
      $this->cacheRoles();
      $perms = [];
      foreach ($this->cachedRoles as $role) {
        $rolePerms = $role->permissions->all();
        foreach ($rolePerms as $rolePerm) {
          if (!in_array($rolePerm->name, $perms)) {
            $perms[] = $rolePerm->name;
          }
        }
      }
      $this->cachedPerms = $perms;
    }
  }

  /**
   * Cache all roles from the database in the instance properties
   *
   * @return void
   */
  protected function cacheRoles() {
    if (!$this->cachedRoles) {
      $this->cachedRoles = $this->roles->all();
    }
  }

  /**
   * Return a list of all role names for this user
   *
   * @return string[]
   */
  public function getRoleNames() {
    $this->cacheRoles();
    $roleNames = [];
    foreach ($this->cachedRoles as $role) {
      $roleNames[] = $role->name;
    }
    return $roleNames;
  }

  /**
   * Determine if user has specified permission
   *
   * @param string $perm Permission string defined in database
   * @return boolean
   */
  public function hasPerm($perm) {
    if ($this->isAdmin()) {
      return true;
    }
    $this->cachePerms();
    return in_array($perm, $this->cachedPerms);
  }

  /**
   * Determine if user is administrator
   *
   * @return boolean
   */
  public function isAdmin() {
    $this->cacheRoles();
    foreach ($this->cachedRoles as $role) {
      if ($role->isAdmin) {
        return true;
      }
    }
    return false;
  }
}
