<?php

/**
 * TaskTag model
 */
namespace Totodo\Model;

/**
 * TaskTag model used for connecting tasks and tags
 */
class TaskTag extends \Defiant\Model {
  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'task' => [
      'type' => '\Defiant\Model\ForeignKeyField',
      'model' => '\Totodo\Model\Task',
    ],
    'tag' => [
      'type' => '\Defiant\Model\ForeignKeyField',
      'model' => '\Totodo\Model\Tag',
    ],
  ];
}
