<?php

/**
 * Task model
 */
namespace Totodo\Model;

/**
 * Tag model used for storing tasks
 */
class Task extends \Defiant\Model {
  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'name' => '\Defiant\Model\VarcharField',
    'description' => '\Defiant\Model\TextField',
    'status' => [
      'type' => '\Defiant\Model\ForeignKeyField',
      'model' => '\Totodo\Model\TaskStatus',
      'default' => 1,
    ],
    'due' => [
      'type' => '\Defiant\Model\DateField',
      'isNull' => true,
    ],
    'archived' => [
      'type' => '\Defiant\Model\BooleanField',
      'default' => false
    ],
    'author' => [
      'type' => '\Defiant\Model\ForeignKeyField',
      'model' => 'Totodo\Model\User',
    ],
    'assigned' => [
      'type' => '\Defiant\Model\ForeignKeyField',
      'model' => 'Totodo\Model\User',
      'isNull' => true,
    ],
    'tags' => [
      'type' => '\Defiant\Model\ManyToManyField',
      'trough' => 'Totodo\Model\TaskTag',
      'model' => 'Totodo\Model\Tag',
      'fk' => 'task',
      'via' => 'tag',
    ],
    'comments' => [
      'type' => '\Defiant\Model\OneToManyField',
      'model' => 'Totodo\Model\TaskComment',
      'fk' => 'task',
    ],
    'changes' => [
      'type' => '\Defiant\Model\OneToManyField',
      'model' => 'Totodo\Model\TaskChange',
      'fk' => 'task',
    ],
  ];

  /**
   * Override default save method to log task creation as a change.
   * @returns Task
   */
  public function save() {
    $wasStored = $this->isStored();
    parent::save();
    if (!$wasStored) {
      $this->log($this->author, TaskChange::ACTION_CREATE, null, ['name' => $this->name]);
    }
    return $this;
  }

  /**
   * Create TaskChange corresponding with this tasks changes.
   * @param \Totodo\Model\User $author Change author
   * @param int $actionType Action type constant from TaskChange class
   * @param Array $previousState Task state before change
   * @param Array $currentState Task state after change
   * @returns TaskChange
   */
  public function log(
    \Totodo\Model\User $author,
    $actionType,
    $previousState = [],
    $currentState = []
  ) {
    $connector = \Totodo\Model\TaskChange::getConnector();
    $item = $connector->create([
      'taskId' => $this->id,
      'authorId' => $author->id,
      'type' => $actionType,
      'previousState' => $previousState,
      'currentState' => $currentState,
    ]);
    if ($actionType === TaskChange::ACTION_CREATE) {
      $item->createdAt = $this->createdAt;
    }
    return $item->save();
  }
}
