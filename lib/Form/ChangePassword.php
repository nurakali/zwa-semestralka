<?php

/** Change password form */
namespace Totodo\Form;

/**
 * Form validator used when changing user passwords
 */
class ChangePassword extends \Defiant\View\Form {
  /** @var \Defiant\Model\Field[] $fields Validated fields */
  protected static $fields = [
    'password' => '\Defiant\Model\PasswordField',
    'newPassword' => '\Defiant\Model\PasswordField',
    'newPasswordCheck' => '\Defiant\Model\PasswordField',
  ];

  /** @var \Totodo\Model\User $user User against which will be validated */
  protected $user;

  /** @var bool $checkOld Should old password be validated */
  protected $checkOld = true;

  /**
   * Create instance of the Form Validator
   *
   * @param \Totodo\Model\User $user Form will be validated against this user
   * @param bool $checkOld Should old password be checked
   * @return \Defiant\View\Form
   */
  public function __construct(\Totodo\Model\User $user, $checkOld = true) {
    $this->user = $user;
    $this->checkOld = $checkOld;
  }

  /**
   * Get all the validated fields. Skips old password field when it is not meant
   * to be validated.
   *
   * @see $checkOld
   * @return \Defiant\Model\Field[]
   */
  public function getInstanceFields() {
    $fields = parent::getInstanceFields();
    if (!$this->checkOld) {
      array_shift($fields);
    }
    return $fields;
  }

  /**
   * Validate submited data.
   *
   * @param \Defiant\Http\Request $request
   * @throws \Defiant\View\ValidationErrorCollection when there is a validation error
   * @return array
   */
  public function getValidatedData(\Defiant\Http\Request $request) {
    $values = parent::getValidatedData($request);
    $errors = [];

    if ($this->checkOld) {
      $passwordField = $this->user->getField('password');
      $hashedPassword = $passwordField->hashValue($values['password']);

      if ($hashedPassword !== $this->user->password) {
        $errors[] = new \Defiant\View\ValidationError(sprintf('Incorrect old password'), 'password');
      }
    }

    if ($values['newPassword'] !== $values['newPasswordCheck']) {
      $errors[] = new \Defiant\View\ValidationError(sprintf('Passwords must be the same'), 'newPasswordCheck');
    }

    if (sizeof($errors) > 0) {
      throw new \Defiant\View\ValidationErrorCollection($errors, []);
    }

    return $values;
  }
}
