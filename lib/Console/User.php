<?php

/**
 * Manipulate users from CLI
 */
namespace Totodo\Console;

/**
 * CLI User helper module, allows managing users in command using Defiant
 *
 * command line runner.
 * @throws Defiant\Model\FieldError when given passwords that are not matching.
 */
class User extends \Defiant\Console\Module {
  const callsign = 'user';

  /**
   * Get a password from the shell.
   * This function works on *nix systems only and requires shell_exec and stty.
   *
   * @param  boolean $stars Wether or not to output stars for given characters
   * @return string
   */
  public static function getPassword($stars = false) {
    // Get current style
    $oldStyle = shell_exec('stty -g');

    if ($stars === false) {
      shell_exec('stty -echo');
      $password = rtrim(fgets(STDIN), "\n");
    } else {
      shell_exec('stty -icanon -echo min 1 time 0');

      $password = '';
      while (true) {
        $char = fgetc(STDIN);

        if ($char === "\n") {
          break;
        } else if (ord($char) === 127) {
          if (strlen($password) > 0) {
              fwrite(STDOUT, "\x08 \x08");
              $password = substr($password, 0, -1);
          }
        } else {
          fwrite(STDOUT, "*");
          $password .= $char;
        }
      }
    }

    // Reset old style
    shell_exec('stty ' . $oldStyle);
    return $password;
  }

  /**
   * Create admin user with password immediately able to log in
   *
   * @param Commando\Command $cmd
   * @return void
   */
  public function createadmin($cmd = null) {
    $cmd->argument()
      ->referToAs('username')
      ->description('User name')
      ->require(true);
    $cmd->parse();
    $username = $cmd[1];
    echo 'Password: ';
    $password = self::getPassword();
    echo "\n";
    echo 'Repeat Password: ';
    $passwordCheck = self::getPassword();
    echo "\n";

    if ($password !== $passwordCheck) {
      throw new \Defiant\Model\FieldError('Passwords do not match');
    }

    $user = $this->runner->models->user->objects
      ->filter(['email' => $username])
      ->first();

    if ($user) {
      $user->password = $password;
      $user->save();
    } else {
      $this->runner->models->user->create([
        'email' => $username,
        'password' => $password,
      ]);
    }
  }
}
