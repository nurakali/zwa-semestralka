<?php

require_once 'vendor/autoload.php';
require_once 'routes.php';
require_once 'lib/Console/index.php';
require_once 'lib/Model/index.php';

/**
 * Produce config for Defiant framework
 * @return void
 */
function configDefiant() {
  $defaultConfig = [
    'database' => [
      'main' => [
        'dsn' => 'sqlite:./db.sqlite',
        'user' => null,
        'password' => null,
      ],
    ],
    'models' => \Defiant\Model::getAncestors(),
    'routes' => getRoutes(),
    'secretKey' => '123',
    'userClass' => 'Totodo\Model\User',
    'viewUnauthorized' => ['\Totodo\View\Login', 'index'],
  ];
  if (file_exists('./localSettings.php')) {
    include('./localSettings.php');
    return array_merge($defaultConfig, $defiantConfig);
  }
  return $defaultConfig;
}
