# Klient (17/15)

## ☑ Javascript (4/4)

### ☑ Kontrola formuláře pomocí JavaScriptu on submit (povinné)

Všechny formuláře jsou validované na backendu a případné chyby vstupu se vypisují v oku příjemné vzdálenosti od příslušných formulářových polí.

### ☑ Kontrola formuláře na všech elementech (2)

Všechna formulářová pole prochází JS validací před odesláním na server, pokud se na ně vztahují nějaká pravidla validace.

* `input[required]` Požaduje nějakou hodnotu
* `input[type=email]` Požaduje hodnotu ve tvaru e-mailové adresy
* `input[type=date]` Požaduje datum ve formátu ISO-8601

### ☑ Validace pomocí HTML5 (1)

*Pozn. atributy required a pattern na všech elementech*

Všechny HTML5 inputy, které jsou od serveru požadované obsahují atributy required. Aatribut pattern je použit u `[type=date` inputu jako fallback pro prohlížeče, které tento type nepodporují.

### ☑ Funkční na prohlížečích Firefox a Chrome (povinné)

Doplňkový JavaScriptový klient byl otestován v prohlížečích Firefox a Google Chrome a funguje bez problémů.

### ☑ Stránka použitelná i bez JavaScriptu (povinné)

*Pozn. Ve zvláštních případech lze udělit zápočet i bez této podmínky. Například když je aplikace SPA*

Celá aplikace je od začátku psaná bez JavaScriptu a postavená tak, aby se dala použít i s vypnutým JavaScriptem. Veškeré skripty jsou pouze doplňky usnadňující přístupnost webu.

### ☑ Zajímavost v JavaScriptu (1)

*Pozn. Na posouzení cvičícího*

Na úvodní stránce byl pro zajímavost vytvořen graf uživatelské aktivity.

V komentářích a popisech úkolů funguje Markdown, včetně obarvování syntaxe vloženého kódu.

Datum a čas je na prohlížeči překládán do lokalizované podoby, která se řídí nastavením prohlížeče. Při změně nastavení preference jazyka dojde automaticky k překreslení stránky.

## ☑ Validnost HTML (6/6)

### ☑ Bez chyb

*Pozn. Kontrola na https://validator.w3.org/*

Zkontroloval jsem všechny dynamické stránky a neodhalil jsem žádnou chybu od validátoru.

### ☑ Bez warningů (1)

HTML kód aplikace je validní a bez warningů, s jedinou výjimkou, kterou jsem se rozhodl ignorovat. Validátor varuje před použitím `[input="date"]` kvůli podpoře prohlížečů, avšak v zadání je pouze podpora Firefox a Chrome. Vzhledem k tomu, že oba dva tyto prohlížeče již plně podporují `[input="date"]`, považuji tento warning za bezpředmětný.

### ☑ Formuláře jsou přístupné/accessible (2)

*Pozn. VŠECHNA formulářová políčka mají label*

Všechna formulářová pole mají label s atributem for, který odkazuje přímo k danému poli. Po kliknutí na label tím pádem přejde focus na příslušné formulářové pole.

### ☑ HTML 5 (3)

*Pozn. Použití html tagů: nové formulářové prvky, nové sémantické prvky*

* Pro definování navigace je v aplikaci použit tag `<nav>`;
* U zadávání data plnění úkolu (Due Date) byl použit `<input type="date" />`.
* U zadávání emailu byl použit `<input type="email" />`.
* Každý výpis data a času je zprostředkován tagem `<time />` včetně atributu datetime.

## ☑ CSS (7/7)

### ☑ Nepoužívá se formátování pomocí HTML elementů (povinné)

Aplikace není závislá na výchozích stylech HTML elementů. Žádné elementy jako `<b>`, `<i>`, `<small>` a `<big>` apod.

### ☑ Styl není přímo u elementu (povinné)

Všechny styly jsou nalinkované v oddělených stylesheetech v hlavičce stránky.

### ☑ Styly mimo HTML dokument pomocí <link> (2)

Všechny styly jsou nalinkované v oddělených stylesheetech v hlavičce stránky.

### ☑ Skinovatelnost (2)

*Pozn. Aplikace bude mít více vzhledů na výběr. Vzhledy budou realizované pomocí CSS. Jednotlivé vzhledy se liší barevně i rozložením. Změna vzhledu je trvalá.*

Uživatel si může vybrat z celkem 19 různých témat podle toho, které víc vyhovuje jeho vizuálním požadavkům.

### ☑ Styl pro tisk (2)

Aplikace má připravený styl pro tisk, který skrývá navigaci a prvky, které nesouvisí s hlavním obsahem.

### ☑ Zajímavost v CSS (1)

*Pozn. mediaqueries, zajímavý efekt. podmínka: uvedeno v dokumentaci*

Aplikace je responzivní a funkční nezávisle na velikosti displeje.

Kód vložený v komentářích je obarvený podle vybrané syntaxe jazyka.

Název aplikace v pravém horním rohu se při načtení stránky zvýrazní animací a dále už neruší.


# ☑ Server (19/15)

## ☑ Validace (8/8)

### ☑ Odolnost proti "zlému uživateli" - výpis do stránky (povinné)

*Pozn. Aplikace nespadne při výpisu dat do stránky (XSS)*

Aplikace má ošetřené vstupy na více úrovních a je tak odolná i proti SQL Injection. Všechny uživatelské vstupy prochází funkcí na převod speciálních znaků na HTML entity.

### ☑ Odolnost proti dvojímu odeslání stejného formuláře (3)

*Pozn. Reload nebo zpět nezpůsobí opětovné provedení akce, např. zapsání dat*

Aplikace posílá s každým formulářem povinně CSRF token. Nestane se tedy, že by náhodou jen tak někdo poslal data na server a ten je přijal. Po úspěšném zpracování formuláře vždy dochází k přesměrování uživatele na stránku se zprávou o výsledku zpracování. Adresa, na které se zpracovává formulář se tak nedostane do historie. Při použití tlačítka zpět se uživatel vrací k formuláři před odesláním. Při "reloadu" se formulář podruhé nezpracuje.

### ☑ Správné vyhodnocení a zobrazení chyb (3)

*Pozn. Obor hodnot, různý formát čísel, ořezávání mezer, mezní a nesmyslné hodnoty. Vše na serveru!*

Aplikace vypisuje chyby v závislosti na definici polí. Například při přiřazování uživatelů k úkolům provádí kontrolu, zda je ID přirazovaného uživatele platné. Stejně tak během přiřazování tagů k úkolům a nebo uživatelských skupin k uživateli.

### ☑ Neztrácí se uživatelem vyplněné hodnoty (povinné)

*Pozn. Nesprávně vyplněný formulář bude odmítnut a vrácen k opravě. Veškerá data zůstanou předvyplněna. Jedinou výjimkou jsou pole s heslem.*

Formuláře, které neprojdou validací na serveru jsou vráceny zpět s předvyplněnými hodnotami. Chyby při zpracování jsou viditelně umístěné poblíž příslušných polí.

### ☑ Jasné rozlišení povinných a nepovinných údajů (2)

Povinné údaje mají popisek tučným fontem, navíc je u popisku hvězdička. Každý formulář, který ma nějaká povinná pole potom má vysvětlivku, která uživateli oznamuje, že pole označená hvězdičkou jsou povinná.


## ☑ General (11/11)

### ☑ Přihlašování uživatelů (povinné)

Uživatel se musí přihlásit pomocí obvyklého formuláře s uživatelským jménem a heslem, aby mohl pracovat s úkoly.

### ☑ Hesla nejsou v plain textu (povinné) (1)

Hesla v databázi jsou zahashovaná za pomoci algoritmu sha1 a přidanou solí.

### ☑ Hesla jsou osolená (2)

Hesla v databázi jsou zahashovaná za pomoci algoritmu sha1 a přidanou solí.

### ☑ Zpracování chyb (povinné)

*Zejména při práci se souborovým systémem*

Systém zpracovává chyby a v případě problému informuje uživatele v podobě speciální chybové stránky.

Speciální případ je pokud se nepřihlášený uživatel snaží dostat na zabezpečenou stránku, potom server uživateli nabídne přihlašovací formulář.

### ☑ Stránkované seznamy položek (povinné)

Seznamy úkolů a seznamy uživatelů jsou číslované.

### ☑ Filtry (2)

*Korektní spolupráce se stránkováním*

Seznamy úkolů obsahují jednoduchý filtr založený na odkazech. Pokud se změní filtr, vymaže se stránkování (UX feature). Ve vyfiltrovaném seznamu jde teprve procházet stránky

### ☑ Ajax (2)

Data pro graf na úvodní stránce se načítají pomocí AJAXu.

### ☑ Šablony (2)

Aplikace využívá šablonovacího jazyka [Phug](https://www.phug-lang.com/).

### ☑ Zajímavost (2)

*Použití webového frameworku (např. NETTE, Zend, Symfony), další zajímavost. Podmínka: musí být zdokumentováno.*

Aplikace je postavená na webovém frameworku [Defiant](https://github.com/just-paja/defiant-core), který byl napsaný speciálně pro účely této aplikace. K natažení závislostí aplikace využívá [Composer](https://getcomposer.org/). Aplikace je schopná vytvořit si sama lokální SQLite databázi a pracovat bez přístupu k databázovému serveru. Aplikace má připravená testovací data v souboru initialData.json, stačí je nalít do databáze, viz README.md.

# Dokumentace (8)

## ☑ Zadání (povinné)

*Pozn. Dokument musí mít všechny následující části: popis úlohy, uživ. příručka, popis implementace, popis úložiště dat. Formální zadání tak, jako by bylo od externího zadavatele. Zadání bude obsahovat i akceptační podmínky.*

Viz oddělený dokument [Zadání](./zadani.md) v repozitáři projektu.

## [] Uživatelská příručka (1/2)

### ☑ Popis funkčnosti webu, Manuál (1)

Viz oddělený dokument [README.cs.md](./README.cs.md) v repozitáři projektu.

### [] Ukázky UI webu, UI bude komentováno, popsáno (1)

Nesplněno

## ☑ Popis implementace (6/6)

### ☑ Popis hlavních rysů programu (2)

*Pozn. (funkčnost hlavních skriptů), Popis architektury, obsluha formulářů, zabezpečení, algoritmy zpracování dat, UML, popis využití frameworků.*

Viz oddělený dokument [README.cs.md](./README.cs.md) v repozitáři projektu.

### ☑ Dokumentace ve zdrojovém kódu (povinné, 2)

Zdrojový kód obsahuje komentáře použitelné s PHPDoc. Dokumentace je vygenerovaná [a publikovaná na serveru wa.toad.cz](https://wa.toad.cz/~zakpave4/totodo-doc).

### ☑ Vygenerovaná dokumentace ze zdrojového kódu (2)

*Pozn. Např. PHPDoc, PHPDocumentor, Doxygen, Apigen...*

K vygenerování dokumentace se používá PHPDoc, který je nainstalován spolu se závislostmi pomocí Composeru. Dokumentace je vygenerovaná [a publikovaná na serveru wa.toad.cz](https://wa.toad.cz/~zakpave4/totodo-doc).
